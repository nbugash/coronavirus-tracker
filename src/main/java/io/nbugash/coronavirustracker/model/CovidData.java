package io.nbugash.coronavirustracker.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * Short description of the class
 *
 * @author nbugash
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CovidData {
    private String province;
    private LocalDate date;
    @JsonProperty(value = "total_cases")
    @SerializedName(value = "total_cases")
    private int totalCases;
    @JsonProperty(value = "total_vaccinated")
    @SerializedName(value = "total_vaccinated")
    private int totalVaccinated;

}
