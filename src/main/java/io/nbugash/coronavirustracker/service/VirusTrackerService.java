package io.nbugash.coronavirustracker.service;

import io.nbugash.coronavirustracker.model.CovidData;

import java.io.IOException;
import java.util.List;

/**
 * Short description of the interface
 *
 * @author nbugash
 */
public interface VirusTrackerService {
    void fetch() throws IOException, InterruptedException;
    List<CovidData> getCovidDataList();
    int getTotalCases();
    int getTotalNumOfPeopleVaccinated();
}
