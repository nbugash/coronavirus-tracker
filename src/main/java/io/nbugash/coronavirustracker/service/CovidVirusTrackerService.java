package io.nbugash.coronavirustracker.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.nbugash.coronavirustracker.adapters.LocalDateAdapter;
import io.nbugash.coronavirustracker.dto.ApiCovidTrackerResponse;
import io.nbugash.coronavirustracker.model.CovidData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Short description of the class
 *
 * @author nbugash
 */
@Service
@Slf4j
public class CovidVirusTrackerService implements VirusTrackerService {

    @Value("${virustracker.url}")
    private String url;
    private List<CovidData> covidDataList = new ArrayList<>();

    @Override
    @PostConstruct
    @Scheduled(cron = "* * * 1 * *")
    @Async
    public void fetch() throws IOException, InterruptedException {
        log.debug("Fetching data from {}", url);
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
                .create();
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .header("Accept", MediaType.APPLICATION_JSON_VALUE)
                .uri(URI.create(url))
                .build();

        HttpResponse<String> httpResponse = client.send(request, HttpResponse.BodyHandlers.ofString());
        ApiCovidTrackerResponse apiCovidTrackerResponse = gson.fromJson(httpResponse.body(), ApiCovidTrackerResponse.class);
        covidDataList = apiCovidTrackerResponse.getData();
        log.debug(apiCovidTrackerResponse.toString());
    }

    @Override
    public List<CovidData> getCovidDataList() {
        return covidDataList;
    }

    @Override public int getTotalCases() {
        return covidDataList.stream().mapToInt(CovidData::getTotalCases).sum();
    }

    @Override public int getTotalNumOfPeopleVaccinated() {
        return covidDataList.stream().mapToInt(CovidData::getTotalVaccinated).sum();
    }
}
