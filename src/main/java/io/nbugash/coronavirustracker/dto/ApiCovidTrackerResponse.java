package io.nbugash.coronavirustracker.dto;

import io.nbugash.coronavirustracker.model.CovidData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Short description of the class
 *
 * @author nbugash
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiCovidTrackerResponse {
    private List<CovidData> data;
}
