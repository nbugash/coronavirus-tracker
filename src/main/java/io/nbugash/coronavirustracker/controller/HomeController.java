package io.nbugash.coronavirustracker.controller;

import io.nbugash.coronavirustracker.model.CovidData;
import io.nbugash.coronavirustracker.service.VirusTrackerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Short description of the class
 *
 * @author nbugash
 */
@Controller
@RequiredArgsConstructor
public class HomeController {

    private final VirusTrackerService service;

    @RequestMapping("/")
    public String home(Model model) {
        model.addAttribute("covidDataList", service.getCovidDataList());
        model.addAttribute("totalVaccinated", service.getTotalNumOfPeopleVaccinated());
        model.addAttribute("totalCases", service.getTotalCases());
        return "home";
    }
}
