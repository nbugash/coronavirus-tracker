# README
## Required software
* Java 16
* Docker (with Docker compose)
* Maven (optional)

## Creating the image
In Mac (or Linux)
```bash
export REPOSITORY='' ;\
export TAG='' ;\
./mvnw spring-boot:build-image \
  -Dspring-boot.build-image.imageName=${REPOSITORY}/${TAG} \
  -DskipTests
```

In windows
```powershell
$env:REPOSITORY=''; `
$env:TAG=''; `
.\mvnw.cmd spring-boot:build-image `
    '-Dspring-boot.build-image.imageName=%REPOSITORY%/%TAG%' `
    '-DskipTests' 
```
## Starting the application
First update the values in the `.env` file with the correct values entered in when building the image

Next run the following command
```bash
docker compose up -d
```
